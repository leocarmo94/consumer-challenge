@extends('documentation.layout.main')

@section('content')

    <header id="header" class="header">
        <div class="container">
            <div class="branding">
                <h1 class="logo">
                    <a href="/">
                        <span aria-hidden="true" class="icon_documents_alt icon"></span>
                        <span class="text-highlight">Consumer</span><span class="text-bold">Challenge</span>
                    </a>
                </h1>
            </div><!--//branding-->
            <ol class="breadcrumb">
                <li class="breadcrumb-item">Documentação</li>
                <li class="breadcrumb-item">API</li>
                <li class="breadcrumb-item active">Utilização</li>
            </ol>
        </div><!--//container-->
    </header><!--//header-->
    <div class="doc-wrapper">
        <div class="container">
            <div id="doc-header" class="doc-header text-center">
                <h1 class="doc-title"><i class="icon fa fa-paper-plane"></i> Utilização da API</h1>
                <div class="meta"><i class="far fa-clock"></i> Última atualização: 2018-09-21</div>
            </div><!--//doc-header-->
            <div class="doc-body row">

                <div class="doc-content col-md-9 col-12 order-1">
                    <div class="content-inner">

                        <section id="api-section" class="doc-section">

                            <div class="callout-block callout-info">
                                <div class="icon-holder">
                                    <i class="fas fa-coffee"></i>
                                </div><!--//icon-holder-->
                                <div class="content">
                                    <h4 class="callout-title">API RESTful</h4>
                                    <p>
                                        Endpoint base: <b>{{ url()->current() }}/api</b>
                                    </p>
                                </div>
                            </div>

                        </section>

                        <section id="auth-section" class="doc-section">

                            <h2 class="section-title">Autenticação</h2>

                            <div class="section-block">
                                <p>
                                    Para a autenticação da API, a lib <a href="https://github.com/tymondesigns/jwt-auth" target="_blank">JWTAuth</a>
                                    foi utilizada para trazer segurança ao acesso das informações.
                                </p>
                            </div>

                            <div class="section-block" id="auth-usage">
                                <h3>Utilização</h3>
                                <p>
                                    Depois de realizar a autenticação, todas as outras requisições solicitam um <b>Header Authorization</b> no seguinte formato:
                                </p>
                                <pre><code class="language-markup">Authorization: Bearer __TOKEN__</code></pre>
                            </div>

                            <div class="section-block" id="auth-routes">
                                <h3>Rotas</h3>
                                <div class="table-responsive">
                                    <table class="table table-bordered">
                                        <thead>
                                        <tr>
                                            <th>Método</th>
                                            <th>URI</th>
                                            <th>Params</th>
                                            <th>Header</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <tr>
                                            <td><label class="badge badge-info">POST</label></td>
                                            <td>/auth</td>
                                            <td>email, password</td>
                                            <td></td>
                                        </tr>
                                        <tr>
                                            <td><label class="badge badge-info">POST</label></td>
                                            <td>/auth/refresh</td>
                                            <td></td>
                                            <td>Authorization</td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div><!--//table-responsive-->
                            </div>

                            <div class="section-block" id="auth-route-auth">
                                <h4>
                                    <label class="badge badge-info">POST</label>
                                    /auth
                                </h4>
                                <p>Rota para autenticação na aplicação. Ao enviar os parametros obrigatórios, um token será retornado para que seja utilizado em outras requisições.</p>

                                <div class="table-responsive">
                                    <table class="table table-bordered">
                                        <thead>
                                        <tr>
                                            <th>Parâmetro</th>
                                            <th>Tipo</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <tr>
                                            <td><b class="text-danger">*</b>email</td>
                                            <td>string</td>
                                        </tr>
                                        <tr>
                                            <td><b class="text-danger">*</b>password</td>
                                            <td>string</td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div><!--//table-responsive-->

                                <p class="font-weight-bold">
                                    Exemplo de retorno:
                                </p>
                                <pre><code class="language-markup">{
    "token": "__TOKEN__"
}</code></pre>

                                <div class="callout-block callout-info">
                                    <div class="icon-holder">
                                        <i class="fas fa-clock"></i>
                                    </div><!--//icon-holder-->
                                    <div class="content">
                                        <h4 class="callout-title">Duração do token</h4>
                                        <p>
                                            Depois de gerado, o token tem a duração de <b>5 minutos</b>. Após este tempo ele não será aceito em novas requisições.
                                        </p>
                                    </div>
                                </div>

                            </div>

                            <div class="section-block" id="auth-route-refresh">
                                <h4>
                                    <label class="badge badge-info">POST</label>
                                    /auth/refresh
                                </h4>
                                <p>Rota para renovar o token para novas requisições.</p>

                                <p class="font-weight-bold">
                                    Exemplo de retorno:
                                </p>
                                <pre><code class="language-markup">{
    "token": "__TOKEN__"
}</code></pre>
                            </div>

                        </section>

                        <section id="base-a-section" class="doc-section">

                            <h2 class="section-title">Base A</h2>

                            <div class="section-block">

                                <div class="callout-block callout-info">
                                    <div class="icon-holder">
                                        <i class="fas fa-lock"></i>
                                    </div><!--//icon-holder-->
                                    <div class="content">
                                        <h4 class="callout-title">Header Authorization</h4>
                                        <p>
                                            Todas as rotas necessitam de um token de autorização.
                                            Verifique a seção de <a class="scrollto font-weight-bold" href="#auth-section">Autenticação</a>.
                                        </p>
                                    </div>
                                </div>

                            </div>

                            <div class="section-block" id="base-a-routes">
                                <h3>Rotas</h3>
                                <div class="table-responsive">
                                    <table class="table table-bordered">
                                        <thead>
                                        <tr>
                                            <th>Método</th>
                                            <th>URI</th>
                                            <th>Params</th>
                                            <th>Header</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <tr>
                                            <td><label class="badge badge-info">POST</label></td>
                                            <td>/base_a/information</td>
                                            <td>cpf, name, address, count</td>
                                            <td>Authorization</td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div><!--//table-responsive-->
                            </div>

                            <div class="section-block" id="base-a-infos">
                                <h4>
                                    <label class="badge badge-info">POST</label>
                                    /base_a/information
                                </h4>
                                <p>Rota para retornar informações de dividas de entidades.</p>

                                <div class="table-responsive">
                                    <table class="table table-bordered">
                                        <thead>
                                        <tr>
                                            <th>Parâmetro</th>
                                            <th>Tipo</th>
                                            <th>Descrição</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <tr>
                                            <td>cpf</td>
                                            <td>string</td>
                                            <td>Parâmetro para filtro por CPF</td>
                                        </tr>
                                        <tr>
                                            <td>name</td>
                                            <td>string</td>
                                            <td>Parâmetro para filtro por nome do cliente</td>
                                        </tr>
                                        <tr>
                                            <td>address</td>
                                            <td>string</td>
                                            <td>Parâmetro para filtro por endereço do cliente</td>
                                        </tr>
                                        <tr>
                                            <td>count</td>
                                            <td>int</td>
                                            <td>Parâmetro para limitar a quantidade de resultados retornados</td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div><!--//table-responsive-->

                                <p class="font-weight-bold">
                                    Exemplo de retorno:
                                </p>
                                <pre><code class="language-markup">[
    {
        "id": 1,
        "cpf": "50065558959",
        "name": "Dr. Elias Thales Espinoza",
        "address": "57476-565, R. Santana, 227. Santa Norma - RR",
        "created_at": "2018-09-21 12:31:11",
        "updated_at": "2018-09-21 12:31:11",
        "debits": [
            {
                "id": 1,
                "basic_information_id": 1,
                "title": "Santa João",
                "description": "Lorem ipsum",
                "company": "Padilha e Maia e Associados",
                "amount": "1674.24",
                "created_at": "2018-09-21 12:31:12",
                "updated_at": "2018-09-21 12:31:12"
            },
            {
                ...
            }
        ]
    },
    {
        ...
    }
]</code></pre>

                            </div>

                        </section>

                        <section id="base-b-section" class="doc-section">

                            <h2 class="section-title">Base B</h2>

                            <div class="section-block">
                                <p>
                                    Esta base contém dados para cálculo do <b>Score de Crédito</b>. O Score
                                    de Crédito é um rating utilizado por instituições de crédito (bancos, imobiliárias, etc) quando
                                    precisam analisar o risco envolvido em uma operação de crédito a uma entidade.
                                </p>
                            </div>

                            <div class="section-block">

                                <div class="callout-block callout-info">
                                    <div class="icon-holder">
                                        <i class="fas fa-lock"></i>
                                    </div><!--//icon-holder-->
                                    <div class="content">
                                        <h4 class="callout-title">Header Authorization</h4>
                                        <p>
                                            Todas as rotas necessitam de um token de autorização.
                                            Verifique a seção de <a class="scrollto font-weight-bold" href="#auth-section">Autenticação</a>.
                                        </p>
                                    </div>
                                </div>

                                <div class="callout-block callout-info">
                                    <div class="icon-holder">
                                        <i class="fas fa-info-circle"></i>
                                    </div><!--//icon-holder-->
                                    <div class="content">
                                        <h4 class="callout-title">Cache de dados</h4>
                                        <p>
                                            As requisições desta base são armazenadas em cache. Para limpar o cache de uma requisição utilize o sufixo <b>/fresh</b> na rota.
                                        </p>
                                    </div>
                                </div>

                            </div>

                            <div class="section-block" id="base-b-routes">
                                <h3>Rotas</h3>
                                <div class="table-responsive">
                                    <table class="table table-bordered">
                                        <thead>
                                        <tr>
                                            <th>Método</th>
                                            <th>URI</th>
                                            <th>Params</th>
                                            <th>Header</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <tr>
                                            <td><label class="badge badge-info">POST</label></td>
                                            <td>/base_b/credit_score/<b>cpf</b></td>
                                            <td>cpf</td>
                                            <td>Authorization</td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div><!--//table-responsive-->
                            </div>

                            <div class="section-block" id="base-b-credit">

                                <h4>
                                    <label class="badge badge-info">POST</label>
                                    /base_b/credit_score/<u>cpf</u>
                                </h4>

                                <p>
                                    Rota para retornar um registro sem cache:
                                    <b>/base_b/credit_score/<u>cpf</u>/fresh</b>
                                </p>

                                <div class="table-responsive">
                                    <table class="table table-bordered">
                                        <thead>
                                        <tr>
                                            <th>Parâmetro</th>
                                            <th>Tipo</th>
                                            <th>Descrição</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <tr>
                                            <td>cpf</td>
                                            <td>string</td>
                                            <td>Parâmetro para filtro pelo CPF</td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div><!--//table-responsive-->

                                <p class="font-weight-bold">
                                    Exemplo de retorno:
                                </p>
                                <pre><code class="language-markup">{
    "id": 1,
    "cpf": "53542101700",
    "age": 40,
    "address": "13281-352, Travessa Elizabeth Marques, 64. Bloco Porto Constância - RN",
    "income_source": "INVESTMENT",
    "created_at": "2018-09-21 12:31:14",
    "updated_at": "2018-09-21 12:31:14",
    "properties": [
        {
            "id": 1,
            "credit_score_id": 1,
            "type": "PROPERTY",
            "value": "604195.13",
            "created_at": "2018-09-21 12:31:15",
            "updated_at": "2018-09-21 12:31:15"
        },
        {
            ...
        }
    ]
}</code></pre>

                            </div>

                        </section>

                        <section id="base-c-section" class="doc-section">

                            <h2 class="section-title">Base C</h2>

                            <div class="section-block">
                                <p>
                                    Esta base contém dados para rastrear eventos relacionados
                                    a um determinado CPF.
                                </p>
                            </div>

                            <div class="section-block">

                                <div class="callout-block callout-info">
                                    <div class="icon-holder">
                                        <i class="fas fa-lock"></i>
                                    </div><!--//icon-holder-->
                                    <div class="content">
                                        <h4 class="callout-title">Header Authorization</h4>
                                        <p>
                                            Todas as rotas necessitam de um token de autorização.
                                            Verifique a seção de <a class="scrollto font-weight-bold" href="#auth-section">Autenticação</a>.
                                        </p>
                                    </div>
                                </div>

                                <div class="callout-block callout-info">
                                    <div class="icon-holder">
                                        <i class="fas fa-info-circle"></i>
                                    </div><!--//icon-holder-->
                                    <div class="content">
                                        <h4 class="callout-title">Cache de dados</h4>
                                        <p>
                                            As requisições desta base são armazenadas em cache. Para limpar o cache de uma requisição utilize o sufixo <b>/fresh</b> na rota.
                                        </p>
                                    </div>
                                </div>

                            </div>

                            <div class="section-block" id="base-c-routes">
                                <h3>Rotas</h3>
                                <div class="table-responsive">
                                    <table class="table table-bordered">
                                        <thead>
                                        <tr>
                                            <th>Método</th>
                                            <th>URI</th>
                                            <th>Params</th>
                                            <th>Header</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <tr>
                                            <td><label class="badge badge-info">POST</label></td>
                                            <td>/base_c/cpf_event/<b>cpf</b></td>
                                            <td>cpf</td>
                                            <td>Authorization</td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div><!--//table-responsive-->
                            </div>

                            <div class="section-block" id="base-c-event">

                                <h4>
                                    <label class="badge badge-info">POST</label>
                                    /base_c/cpf_event/<u>cpf</u>
                                </h4>

                                <p>
                                    Rota para retornar um registro sem cache:
                                    <b>/base_c/cpf_event/<u>cpf</u>/fresh</b>
                                </p>

                                <div class="table-responsive">
                                    <table class="table table-bordered">
                                        <thead>
                                        <tr>
                                            <th>Parâmetro</th>
                                            <th>Tipo</th>
                                            <th>Descrição</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <tr>
                                            <td>cpf</td>
                                            <td>string</td>
                                            <td>Parâmetro para filtro pelo CPF</td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div><!--//table-responsive-->

                                <p class="font-weight-bold">
                                    Exemplo de retorno:
                                </p>
                                <pre><code class="language-markup">{
    "id": 1,
    "cpf": "94060884448",
    "created_at": "2018-09-21 12:31:17",
    "updated_at": "2018-09-21 12:31:17",
    "last_purchase": {
        "id": 1,
        "cpf_event_id": 1,
        "company": "Saraiva Comercial Ltda.",
        "address": "35109-643, Largo Tessália, 445. Bloco A\nSanta Aaron - PB",
        "value": "540469.02",
        "created_at": "2018-09-21 12:31:18",
        "updated_at": "2018-09-21 12:31:18"
    },
    "last_bureau_consult": {
        "id": 6,
        "cpf_event_id": 1,
        "bureau": "SERASA",
        "created_at": "2018-09-21 12:31:18",
        "updated_at": "2018-09-21 12:31:18"
    }
}</code></pre>

                            </div>

                        </section>

                    </div><!--//content-inner-->
                </div><!--//doc-content-->

                <div class="doc-sidebar col-md-3 col-12 order-0 d-none d-md-flex">
                    <div id="doc-nav" class="doc-nav">

                        <nav id="doc-menu" class="nav doc-menu flex-column sticky">

                            <a class="nav-link scrollto" href="#auth-section">Autenticação</a>

                            <nav class="doc-sub-menu nav flex-column">
                                <a class="nav-link scrollto" href="#auth-usage">
                                    Utilização
                                </a>
                                <a class="nav-link scrollto" href="#auth-routes">
                                    Rotas
                                </a>
                                <a class="nav-link scrollto" href="#auth-route-auth">
                                    <label class="badge badge-info">POST</label>
                                    /auth
                                </a>
                                <a class="nav-link scrollto" href="#auth-route-refresh">
                                    <label class="badge badge-info">POST</label>
                                    /auth/refresh
                                </a>
                            </nav>

                            <a class="nav-link scrollto" href="#base-a-section">Base A</a>

                            <nav class="doc-sub-menu nav flex-column">
                                <a class="nav-link scrollto" href="#base-a-routes">
                                    Rotas
                                </a>
                                <a class="nav-link scrollto" href="#base-a-infos">
                                    <label class="badge badge-info">POST</label>
                                    /base_a/information
                                </a>
                            </nav>

                            <a class="nav-link scrollto" href="#base-b-section">Base B</a>

                            <nav class="doc-sub-menu nav flex-column">
                                <a class="nav-link scrollto" href="#base-b-routes">
                                    Rotas
                                </a>
                                <a class="nav-link scrollto" href="#base-b-credit">
                                    <label class="badge badge-info">POST</label>
                                    /base_b/credit_score/cpf
                                </a>
                            </nav>

                            <a class="nav-link scrollto" href="#base-c-section">Base C</a>

                            <nav class="doc-sub-menu nav flex-column">
                                <a class="nav-link scrollto" href="#base-c-routes">
                                    Rotas
                                </a>
                                <a class="nav-link scrollto" href="#base-c-event">
                                    <label class="badge badge-info">POST</label>
                                    /base_c/cpf_event/cpf
                                </a>
                            </nav>

                        </nav><!--//doc-menu-->

                    </div>
                </div><!--//doc-sidebar-->

            </div><!--//doc-body-->
        </div><!--//container-->
    </div><!--//doc-wrapper-->

@endsection