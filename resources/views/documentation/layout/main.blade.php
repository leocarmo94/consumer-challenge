<!DOCTYPE html>
<html lang="pt_BR">
<head>
    <title>Documentação - Consumer Challenge API</title>
    <!-- Meta -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">

    <link href='https://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css'>

    <link rel="stylesheet" href="{{ mix('assets/css/documentation.css') }}">

</head>

<body class="body-green">
<div class="page-wrapper">

    @yield('content')

</div><!--//page-wrapper-->

<footer id="footer" class="footer text-center">
    <div class="container">
        <!--/* This template is released under the Creative Commons Attribution 3.0 License. Please keep the attribution link below when using for your own project. Thank you for your support. :) If you'd like to use the template without the attribution, you can buy the commercial license via our website: themes.3rdwavemedia.com */-->
        <small class="copyright">API by <a href="https://leonardocarmo.com.br/">Leonardo Carmo</a> - Theme Designed by <a href="https://themes.3rdwavemedia.com/" target="_blank">Xiaoying Riley</a></small>

    </div><!--//container-->
</footer><!--//footer-->

<script type="text/javascript" src="{{ mix('assets/js/documentation.js') }}"></script>

</body>
</html>

