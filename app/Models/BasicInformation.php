<?php

namespace App\Models;

use App\Services\Api\BaseA\ModelConfiguration;
use Illuminate\Database\Eloquent\Model;
use LeoCarmo\ModelFilter\Traits\FilterableModel;

class BasicInformation extends Model
{

    use FilterableModel;

    /**
     * Allowed fields for filter
     *
     * @var array
     * https://github.com/leocarmo/laravel-eloquent-model-filter
     */
    protected $filterable = [
        'cpf',
        'name' => 'LIKE',
        'address' => 'LIKE',
    ];

    public function __construct(array $attributes = [])
    {
        $this->setConnection(
            ModelConfiguration::connection()
        );

        parent::__construct($attributes);
    }

    public function debits()
    {
        return $this->hasMany(BasicInformationDebit::class, 'basic_information_id');
    }

}
