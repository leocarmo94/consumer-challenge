<?php

namespace App\Models;

use App\Services\Api\BaseB\ModelConfiguration;
use Illuminate\Database\Eloquent\Model;

class CreditScoreProperty extends Model
{

    public function __construct(array $attributes = [])
    {
        $this->setConnection(
            ModelConfiguration::connection()
        );

        parent::__construct($attributes);
    }

}
