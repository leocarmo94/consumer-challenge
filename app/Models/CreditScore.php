<?php

namespace App\Models;

use App\Services\Api\BaseB\ModelConfiguration;
use Illuminate\Database\Eloquent\Model;
use LeoCarmo\ModelFilter\Traits\FilterableModel;

class CreditScore extends Model
{

    use FilterableModel;

    /**
     * Allowed fields for filter
     *
     * @var array
     * https://github.com/leocarmo/laravel-eloquent-model-filter
     */
    protected $filterable = [
        'cpf',
    ];

    public function __construct(array $attributes = [])
    {
        $this->setConnection(
            ModelConfiguration::connection()
        );

        parent::__construct($attributes);
    }

    public function properties()
    {
        return $this->hasMany(CreditScoreProperty::class, 'credit_score_id');
    }

}
