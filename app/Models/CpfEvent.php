<?php

namespace App\Models;

use App\Services\Api\BaseC\ModelConfiguration;
use Illuminate\Database\Eloquent\Model;
use LeoCarmo\ModelFilter\Traits\FilterableModel;

class CpfEvent extends Model
{

    use FilterableModel;

    /**
     * Allowed fields for filter
     *
     * @var array
     * https://github.com/leocarmo/laravel-eloquent-model-filter
     */
    protected $filterable = [
        'cpf',
    ];

    public function __construct(array $attributes = [])
    {
        $this->setConnection(
            ModelConfiguration::connection()
        );

        parent::__construct($attributes);
    }

    public function purchases()
    {
        return $this->hasMany(CpfEventPurchase::class, 'cpf_event_id');
    }

    public function bureauConsults()
    {
        return $this->hasMany(CpfEventBureauConsult::class, 'cpf_event_id');
    }

    public function lastPurchase()
    {
        $this->last_purchase = $this->purchases()->orderByDesc('created_at')->first();
        return $this;
    }

    public function lastBureauConsult()
    {
        $this->last_bureau_consult = $this->bureauConsults()->orderByDesc('created_at')->first();
        return $this;
    }

}
