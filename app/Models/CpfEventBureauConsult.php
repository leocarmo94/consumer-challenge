<?php

namespace App\Models;

use App\Services\Api\BaseC\ModelConfiguration;
use Illuminate\Database\Eloquent\Model;

class CpfEventBureauConsult extends Model
{

    public function __construct(array $attributes = [])
    {
        $this->setConnection(
            ModelConfiguration::connection()
        );

        parent::__construct($attributes);
    }

}
