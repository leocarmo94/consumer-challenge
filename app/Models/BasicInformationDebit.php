<?php

namespace App\Models;

use App\Services\Api\BaseA\ModelConfiguration;
use Illuminate\Database\Eloquent\Model;

class BasicInformationDebit extends Model
{

    public function __construct(array $attributes = [])
    {
        $this->setConnection(
            ModelConfiguration::connection()
        );

        parent::__construct($attributes);
    }

}
