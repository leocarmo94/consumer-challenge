<?php

if (! function_exists('only_numbers')) {

    /**
     * Remove all except numbers 0-9
     * @param $value
     * @return null|string|string[]
     */
    function only_numbers($value) {

        return preg_replace('/[^0-9]/', '', $value);

    }

}
