<?php

namespace App\Repositories\Api\BaseA;

use App\Exceptions\NotFoundSearchModel;
use App\Models\BasicInformation;
use Illuminate\Http\Request;

class BasicInformationRepository
{

    public static function rest(Request $request)
    {

        // clear cpf field
        $filters = array_merge($request->all(), [
            'cpf' => only_numbers($request->input('cpf'))
        ]);

        // get model
        $model = (new BasicInformation);

        // apply filter
        $query = $model->filter($filters);

        // call relations
        $query->with('debits');

        // check if count was defined and apply
        if ($request->has('count')) {
            $query->limit(
                $request->input('count')
            );
        }

        // results
        $models = $query->get();

        // check result
        if ($models) {
            return $models;
        }

        // not found
        throw new NotFoundSearchModel();

    }

}