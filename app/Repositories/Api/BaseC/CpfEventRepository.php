<?php

namespace App\Repositories\Api\BaseC;

use App\Exceptions\NotFoundSearchModel;
use App\Models\CpfEvent;
use Illuminate\Support\Facades\Cache;

class CpfEventRepository
{

    public static function byCpf(string $cpf)
    {

        $cpf = only_numbers($cpf);

        return Cache::remember(
            "api.base_c.cpf_event.byCpf.{$cpf}",
            env('APP_CACHE_MIN_BASE_C'),
            function () use ($cpf) {

                // get model
                $model = (new CpfEvent);

                // apply filter
                $query = $model->filter([
                    'cpf' => $cpf
                ]);

                // results
                $model = $query->first();

                // check result
                if ($model) {
                    return $model->lastPurchase()->lastBureauConsult();
                }

                // not found
                throw new NotFoundSearchModel();

            }
        );

    }

    public static function byCpfFresh(string $cpf)
    {

        $cpf = only_numbers($cpf);

        Cache::forget("api.base_c.cpf_event.byCpf.{$cpf}");

        return self::byCpf($cpf);

    }

}