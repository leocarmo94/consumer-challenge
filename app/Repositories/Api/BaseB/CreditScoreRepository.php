<?php

namespace App\Repositories\Api\BaseB;

use App\Exceptions\NotFoundSearchModel;
use App\Models\CreditScore;
use Illuminate\Support\Facades\Cache;

class CreditScoreRepository
{

    public static function byCpf(string $cpf)
    {

        $cpf = only_numbers($cpf);

        return Cache::remember(
            "api.base_b.credit_score.byCpf.{$cpf}",
            env('APP_CACHE_MIN_BASE_B'),
            function () use ($cpf) {

                // get model
                $model = (new CreditScore);

                // apply filter
                $query = $model->filter([
                    'cpf' => $cpf
                ]);

                // call relations
                $query->with('properties');

                // results
                $model = $query->first();

                // check result
                if ($model) {
                    return $model;
                }

                // not found
                throw new NotFoundSearchModel();

            }
        );

    }

    public static function byCpfFresh(string $cpf)
    {

        $cpf = only_numbers($cpf);

        Cache::forget("api.base_b.credit_score.byCpf.{$cpf}");

        return self::byCpf($cpf);

    }

}