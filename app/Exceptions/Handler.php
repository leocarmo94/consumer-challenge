<?php

namespace App\Exceptions;

use Exception;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that are not reported.
     *
     * @var array
     */
    protected $dontReport = [
        //
    ];

    /**
     * A list of the inputs that are never flashed for validation exceptions.
     *
     * @var array
     */
    protected $dontFlash = [
        'password',
        'password_confirmation',
    ];

    /**
     * @param Exception $exception
     * @return mixed|void
     * @throws Exception
     */
    public function report(Exception $exception)
    {
        parent::report($exception);
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Exception  $exception
     * @return \Illuminate\Http\Response
     */
    public function render($request, Exception $exception)
    {
        $exception_class = lcfirst(class_basename($exception));

        if (method_exists($this, $exception_class)) {
            return $this->$exception_class($request);
        }

        return parent::render($request, $exception);
    }

    public function jWTException()
    {
        return response()->json(['error' => 'could_not_create_token'], 500);
    }

    public function tokenInvalidException()
    {
        return response()->json(['error' => 'invalid_token'], 401);
    }

    public function tokenBlacklistedException()
    {
        return response()->json(['error' => 'invalid_token'], 401);
    }

    public function tokenExpiredException()
    {
        return response()->json(['error' => 'token_expired'], 401);
    }

    public function unauthorizedHttpException()
    {
        return response()->json(['error' => 'token_not_provided'], 401);
    }

    public function methodNotAllowedHttpException()
    {
        return response()->json(['error' => 'method_not_allowed'], 405);
    }

    public function notFoundHttpException()
    {
        return response()->json(['error' => 'endpoint_not_found'], 404);
    }

}
