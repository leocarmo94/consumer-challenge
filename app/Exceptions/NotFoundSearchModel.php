<?php

namespace App\Exceptions;

use Exception;

class NotFoundSearchModel extends Exception
{

    public function render()
    {
        return response()->json(['error' => 'no_content_found'], 204);
    }

}
