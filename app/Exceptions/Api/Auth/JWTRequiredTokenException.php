<?php

namespace App\Exceptions\Api\Auth;

use Exception;

class JWTRequiredTokenException extends Exception
{

    public function render()
    {
        return response()->json(['error' => 'token_is_required'], 401);
    }

}
