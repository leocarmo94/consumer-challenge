<?php

namespace App\Services\Api\Auth;

use App\Exceptions\Api\Auth\JWTRequiredTokenException;
use Illuminate\Http\Request;
use Tymon\JWTAuth\Facades\JWTAuth;

trait AuthService
{

    /**
     * JWT Token
     *
     * @var string
     */
    protected $token;

    /**
     * Get credentials from request
     *
     * @param Request $request
     * @return array
     */
    protected function credentials(Request $request)
    {
        return $request->only('email', 'password');
    }

    /**
     * Attempt to generate token
     *
     * @param Request $request
     * @return mixed
     */
    protected function attempt(Request $request)
    {
        return $this->token = JWTAuth::attempt(
            $this->credentials(
                $request
            )
        );
    }

    /**
     * Return token
     *
     * @return mixed
     * @throws JWTRequiredTokenException
     */
    protected function getToken()
    {
        if ($this->token) {
            return $this->token;
        }

        if ($token = JWTAuth::getToken()) {
            return $this->token = $token;
        }

        throw new JWTRequiredTokenException();
    }

    /**
     * Refresh token
     *
     * @return mixed
     */
    protected function refreshToken()
    {
        return $this->token = JWTAuth::refresh();
    }

}