<?php

namespace App\Http\Controllers\Api\BaseB;

use App\Http\Controllers\Controller;
use App\Repositories\Api\BaseB\CreditScoreRepository;

class CreditScoreController extends Controller
{

    public function show(string $cpf)
    {

        return CreditScoreRepository::byCpf($cpf);

    }

    public function showFresh(string $cpf)
    {

        return CreditScoreRepository::byCpfFresh($cpf);

    }

}
