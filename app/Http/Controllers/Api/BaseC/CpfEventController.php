<?php

namespace App\Http\Controllers\Api\BaseC;

use App\Http\Controllers\Controller;
use App\Repositories\Api\BaseC\CpfEventRepository;

class CpfEventController extends Controller
{

    public function show(string $cpf)
    {

        return CpfEventRepository::byCpf($cpf);

    }

    public function showFresh(string $cpf)
    {

        return CpfEventRepository::byCpfFresh($cpf);

    }

}
