<?php

namespace App\Http\Controllers\Api\BaseA;

use App\Http\Controllers\Controller;
use App\Repositories\Api\BaseA\BasicInformationRepository;
use Illuminate\Http\Request;

class InformationController extends Controller
{

    /**
     * @param Request $request
     * @return \Illuminate\Support\Collection
     * @throws \App\Exceptions\NotFoundSearchModel
     */
    public function index(Request $request)
    {

        return BasicInformationRepository::rest($request);

    }

}
