<?php

namespace App\Http\Controllers\Api\Auth;

use App\Http\Controllers\Controller;
use App\Http\Requests\Api\Auth\AuthRequest;
use App\Services\Api\Auth\AuthService;

class AuthController extends Controller
{

    use AuthService;

    /**
     * Auth user and return token
     *
     * @param AuthRequest $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \App\Exceptions\Api\Auth\JWTRequiredTokenException
     */
    public function auth(AuthRequest $request)
    {

        if ($this->attempt($request)) {

            return response()
                ->json([
                    'token' => $this->getToken()
                ]);

        }

        return response()
            ->json(
                ['error' => 'invalid_credentials'],
                401
            );

    }

    /**
     * Refresh token
     *
     * @return \Illuminate\Http\JsonResponse
     * @throws \App\Exceptions\Api\Auth\JWTRequiredTokenException
     */
    public function refresh()
    {

        // first, get token from headers
        $this->getToken();

        // return new token
        return response()->json([
            'token' => $this->refreshToken()
        ]);

    }

}
