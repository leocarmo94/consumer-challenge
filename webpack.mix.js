const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Documentation
 |--------------------------------------------------------------------------
 |
 */

mix.babel([

    // PRIMARY
    'resources/js/documentation/main/jquery-3.3.1.min.js',
    'resources/js/documentation/main/bootstrap.min.js',

    // LIBS
    'resources/js/documentation/lib/*',

    // Main
    'resources/js/documentation/main/main.js',

], 'public/assets/js/documentation.js').version();

mix.styles([

    // PRIMARY
    'resources/css/documentation/main/bootstrap.min.css',

    // LIB
    'resources/css/documentation/lib/*',

    // Theme
    'resources/css/documentation/main/styles.css',

], 'public/assets/css/documentation.css').version();
