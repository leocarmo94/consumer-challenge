# Consumer Challenge API

Para instalar a aplicação basta seguir os passos abaixo.

## Instalação

Clone este repositório e instale as dependencias:
```sh
composer install
```

Clone o arquivo `.env.example` para `.env` e configure a aplicação:
```sh
cp .env.example .env
``` 

Gere as chaves secretas da aplicação:
```sh
php artisan key:generate
php artisan jwt:secret
``` 

Para simular as bases externas, gere as migrations e depois as seeds para simular dados nas mesmas:
```sh
php artisan migrate --seed
``` 

## Documentação API

Para acessar a documentação da API basta acessar a URL base da aplicação depois de instalada e configurada.

## Créditos

- [Leonardo do Carmo](https://github.com/leocarmo)