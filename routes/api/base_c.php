<?php

Route::post('/cpf_event/{cpf}', 'CpfEventController@show')->name('api.base_c.cpf_event');
Route::post('/cpf_event/{cpf}/fresh', 'CpfEventController@showFresh')->name('api.base_c.cpf_event.fresh');