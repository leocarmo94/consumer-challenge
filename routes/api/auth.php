<?php

Route::post('/', 'AuthController@auth')->name('api.auth');
Route::post('/refresh', 'AuthController@refresh')->name('api.auth.refresh');