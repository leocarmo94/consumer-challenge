<?php

Route::post('/credit_score/{cpf}', 'CreditScoreController@show')->name('api.base_b.credit_score');
Route::post('/credit_score/{cpf}/fresh', 'CreditScoreController@showFresh')->name('api.base_b.credit_score.fresh');