<?php

use Faker\Generator as Faker;

$factory->define(\App\Models\BasicInformation::class, function (Faker $faker) {
    return [
        'cpf' => $faker->numerify('###########'),
        'name' => $faker->name,
        'address' => $faker->address,
    ];
});

$factory->define(\App\Models\BasicInformationDebit::class, function (Faker $faker) {
    return [
        'title' => $faker->city,
        'description' => $faker->paragraphs(2, true),
        'company' => $faker->company,
        'amount' => $faker->randomFloat(2, 1000, 100000),
    ];
});