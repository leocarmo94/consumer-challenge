<?php

use Faker\Generator as Faker;

$factory->define(\App\Models\CpfEvent::class, function (Faker $faker) {
    return [
        'cpf' => $faker->numerify('###########'),
    ];
});

$factory->define(\App\Models\CpfEventPurchase::class, function (Faker $faker) {
    return [
        'company' => $faker->company,
        'address' => $faker->address,
        'value' => $faker->randomFloat(2, 1000, 1000000),
    ];
});

$factory->define(\App\Models\CpfEventBureauConsult::class, function (Faker $faker) {
    return [
        'bureau' => $faker->randomElement(['SERASA', 'OUTROS']),
    ];
});