<?php

use Faker\Generator as Faker;

$factory->define(\App\Models\CreditScore::class, function (Faker $faker) {
    return [
        'cpf' => $faker->numerify('###########'),
        'age' => rand(18, 100),
        'address' => $faker->address,
        'income_source' => $faker->randomElement(['JOB', 'RENT', 'INVESTMENT'])
    ];
});

$factory->define(\App\Models\CreditScoreProperty::class, function (Faker $faker) {
    return [
        'type' => $faker->randomElement(['CAR', 'PROPERTY']),
        'value' => $faker->randomFloat(2, 10000, 1000000),
    ];
});