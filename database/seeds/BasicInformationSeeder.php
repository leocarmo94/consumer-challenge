<?php

use Illuminate\Database\Seeder;

class BasicInformationSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(\App\Models\BasicInformation::class, 50)
            ->create()
            ->each(function ($information) {

                factory(\App\Models\BasicInformationDebit::class, rand(1, 10))
                    ->create([
                        'basic_information_id' => $information->id
                    ]);

            });
    }
}
