<?php

use Illuminate\Database\Seeder;

class CreditScoreSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(\App\Models\CreditScore::class, 50)
            ->create()
            ->each(function ($credit) {

                factory(\App\Models\CreditScoreProperty::class, rand(1, 10))
                    ->create([
                        'credit_score_id' => $credit->id
                    ]);

            });
    }
}
