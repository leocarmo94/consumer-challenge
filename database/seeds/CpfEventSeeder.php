<?php

use Illuminate\Database\Seeder;

class CpfEventSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(\App\Models\CpfEvent::class, 50)
            ->create()
            ->each(function ($event) {

                factory(\App\Models\CpfEventPurchase::class, rand(1, 10))
                    ->create([
                        'cpf_event_id' => $event->id
                    ]);

                factory(\App\Models\CpfEventBureauConsult::class, rand(1, 10))
                    ->create([
                        'cpf_event_id' => $event->id
                    ]);

            });
    }
}
