<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCpfEventsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection(\App\Services\Api\BaseC\ModelConfiguration::connection())
            ->create('cpf_events', function (Blueprint $table) {

                $table->increments('id');

                $table->char('cpf', 11);

                $table->timestamps();

            }
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection(\App\Services\Api\BaseC\ModelConfiguration::connection())
            ->dropIfExists('cpf_events');
    }
}
