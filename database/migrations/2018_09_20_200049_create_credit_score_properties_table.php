<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCreditScorePropertiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection(\App\Services\Api\BaseB\ModelConfiguration::connection())
            ->create('credit_score_properties', function (Blueprint $table) {

                $table->increments('id');

                $table->integer('credit_score_id')->unsigned();
                $table->foreign('credit_score_id')->references('id')->on('credit_scores');

                $table->string('type');
                $table->decimal('value', 10, 2);

                $table->timestamps();

            }
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection(\App\Services\Api\BaseB\ModelConfiguration::connection())
            ->dropIfExists('credit_score_properties');
    }
}
