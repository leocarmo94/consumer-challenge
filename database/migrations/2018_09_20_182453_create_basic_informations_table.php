<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBasicInformationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection(\App\Services\Api\BaseA\ModelConfiguration::connection())
            ->create('basic_informations', function (Blueprint $table) {

                $table->increments('id');

                $table->char('cpf', 11);
                $table->string('name');
                $table->text('address');

                $table->timestamps();

            }
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection(\App\Services\Api\BaseA\ModelConfiguration::connection())
            ->dropIfExists('basic_informations');
    }
}
