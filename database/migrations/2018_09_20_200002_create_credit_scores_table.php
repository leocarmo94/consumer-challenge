<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCreditScoresTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection(\App\Services\Api\BaseB\ModelConfiguration::connection())
            ->create('credit_scores', function (Blueprint $table) {

                $table->increments('id');

                $table->char('cpf', 11);
                $table->integer('age');
                $table->text('address');
                $table->text('income_source');

                $table->timestamps();

            }
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection(\App\Services\Api\BaseB\ModelConfiguration::connection())
            ->dropIfExists('credit_scores');
    }
}
