<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBasicInformationDebitsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection(\App\Services\Api\BaseA\ModelConfiguration::connection())
            ->create('basic_information_debits', function (Blueprint $table) {

                $table->increments('id');

                $table->integer('basic_information_id')->unsigned();
                $table->foreign('basic_information_id')->references('id')->on('basic_informations');

                $table->string('title');
                $table->text('description');
                $table->string('company');
                $table->decimal('amount', 10, 2);

                $table->timestamps();

            }
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection(\App\Services\Api\BaseA\ModelConfiguration::connection())
            ->dropIfExists('basic_information_debits');
    }
}
