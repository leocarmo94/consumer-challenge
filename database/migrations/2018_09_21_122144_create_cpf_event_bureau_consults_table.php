<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCpfEventBureauConsultsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection(\App\Services\Api\BaseC\ModelConfiguration::connection())
            ->create('cpf_event_bureau_consults', function (Blueprint $table) {

                $table->increments('id');

                $table->integer('cpf_event_id')->unsigned();
                $table->foreign('cpf_event_id')->references('id')->on('cpf_events');

                $table->string('bureau');

                $table->timestamps();

            }
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection(\App\Services\Api\BaseC\ModelConfiguration::connection())
            ->dropIfExists('cpf_event_bureau_consults');
    }
}
